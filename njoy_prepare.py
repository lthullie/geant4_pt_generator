import os
import numpy as np
import argparse

parser = argparse.ArgumentParser(fromfile_prefix_chars='@')
parser.add_argument("-njoyp","--njoypath",default="0",
help="Absolute path to executable njoy21.")
parser.add_argument("-ENDFdir","--ENDFdirectory",default="tungsten_test_ENDFB7.1",
help="Name of the data file for which the probability tables should be prepared.")
parser.add_argument("-T","--temperatures",default=293.15,type=float,nargs="+",
help="Temperature or temperatures for the processing of the neutron data.")
parser.add_argument("-p","--precision",default=0.001,type=float,
help="Precision for the reconstruction of the cross section in NJOY.")
parser.add_argument("-probs","--probabilities",default=20,type=int,
help="Number of generated probabilities for each energy point.")
parser.add_argument("-samp","--samples",default=64,type=int,
help="Number of generated samples of resonance ladders for each energy.")
parser.add_argument("-pigzp","--pigzpath",default="0",
help="Absolute path to executable pigz.")
args=parser.parse_args()

datafilename=args.ENDFdirectory
pigzpath=args.pigzpath
if not os.path.exists(os.path.join(pigzpath,"pigz")):
    print('The path to pigz does not contain executable "pigz". The data will be not compressed.' )
    pigzpath="0"

scriptpath=os.path.abspath(os.path.dirname(__file__))
datapath=os.path.join(scriptpath,datafilename)

if not os.path.exists(datapath):
    print('There is no datapath with the name "'+datafilename+'" in the directory with processing scripts.')
    exit()
elif not os.path.exists(datapath+"/NJOY_run"):
    print("There are no results processed by NJOY. Run njoyer.py first.")
    exit()
elif not os.path.exists(datapath+"/G4URRPT"):
    os.mkdir(datapath+"/G4URRPT")
if not os.path.exists(datapath+"/G4URRPT/njoy"):
    os.mkdir(datapath+"/G4URRPT/njoy")
ptdatapath=datapath+"/G4URRPT/njoy"

processeddatapath=os.path.join(scriptpath,datafilename,"NJOY_run")

for diriterator in os.scandir(processeddatapath):
    if diriterator.is_dir():
        # find all directories and their names
        if "_" in diriterator.name:
            splitted_name=diriterator.name.split("_")
            Z=int(splitted_name[0])
            A=int(splitted_name[1])
            m=int(splitted_name[2])
            if m==0:
                new_file_name=str(Z)+"_"+str(A)+"."
            else:
                new_file_name=str(Z)+"_"+str(A)+"_m"+str(m)+"."
            #read probability tables from tape28
            workdir=os.path.join(processeddatapath,diriterator.name)
            if os.path.exists(workdir+"/tape20"):
                os.remove(workdir+"/tape20")
            if not os.path.exists(workdir+"/tape28"):
                print("There was a problem preparing data for "+new_file_name)
                print("Check the output of njoy for this isotope.")
                continue
            with open(workdir+"/tape28") as file:
                for line in file:
                    #for each temperature, there are probability tables in section 2153
                    if line[71:75]=="2153":
                        n_ene=-1
                        temp=-1
                        n_prob=-1
                        lssf=-1
                        line_count=-1
                        new_file_content=""
                        n_prob=int(line[62:66].strip())
                        line=file.readline()
                        number=line[0:11].strip()
                        positive=number.split("+")
                        negative=number.split("-")
                        if len(positive)==2:
                            temp=int(float(positive[0])*10**(int(positive[1])))
                        elif len(negative)==2:
                            temp=int(float(negative[0])*10**(-1*int(negative[1])))
                        n_ene=int(line[62:66].strip())
                        lssf=int(line[31:33].strip())
                        batch=n_prob*6+1
                        theBigM=np.zeros((n_ene,batch))
                        line=file.readline()
                        while (line[71:75]=="2153"):
                            line_count+=1
                            for i in range(6):
                                if (line_count*6+i)>=(batch*n_ene-1):
                                    break
                                number=line[i*11:(i*11)+11].strip()
                                positive=number.split("+")
                                negative=number.split("-")
                                if len(positive)==2:
                                    number=(float(positive[0])*10**(int(positive[1])))
                                elif len(negative)==2:
                                    number=(float(negative[0])*10**(-1*int(negative[1])))
                                elif len(negative)==3:
                                    number=0
                                theBigM[(line_count*6+i)//batch,(line_count*6+i)%batch]=number
                            line=file.readline()
                        #calculate cumulative probabilities
                        for i in range(n_ene):
                            for j in range(n_prob):
                                if j==0:
                                    theBigM[i][1]=theBigM[i][1]
                                elif (j==(n_prob-1)):
                                    theBigM[i][n_prob]=1.0
                                else:
                                    theBigM[i][1+j]=theBigM[i][1+j]+theBigM[i][j]
                        # write new table file
                        with open(os.path.join(ptdatapath,new_file_name+str(temp)+".pt"), 'w') as new_file:
                            new_file.write("{:1.6E}".format(theBigM[0][0])+" "+"{:1.6E}".format(theBigM[-1][0])+" "+"{:1.6G}".format(n_ene)+" "+"{:1.6G}".format(n_prob)+" "+"{:1.6G}".format(lssf)+"\n")
                            for i in range(n_ene):
                                new_file.write("{:1.6E}".format(theBigM[i][0])+"\n")
                                for j in range(n_prob):
                                    new_file.write("{:1.6E}".format(theBigM[i][1+j])+" "+"{:1.6E}".format(theBigM[i][n_prob+1+j])+" "+"{:1.6E}".format(theBigM[i][2*n_prob+1+j])+" "+"{:1.6E}".format(theBigM[i][4*n_prob+1+j])+" "+"{:1.6E}".format(theBigM[i][3*n_prob+1+j])+"\n")

if pigzpath!="0":
    pigz=os.path.join(pigzpath,"pigz")
    for fileiterator in os.scandir(ptdatapath):
        if fileiterator.is_file():
            if ".z" in fileiterator.name:
                continue
            else:
                os.chdir(ptdatapath)
                os.system(pigz+' -z1 < "'+fileiterator.name+'" > "'+fileiterator.name+'.z"')
                os.remove(os.path.join(ptdatapath,fileiterator.name))
                
print("JOB DONE!")
