import os
import argparse
import shutil
import numpy as np

parser = argparse.ArgumentParser(fromfile_prefix_chars='@')
parser.add_argument("-calendfp","--calendfpath",default="0",
help="Absolute path to executable calendf.")
parser.add_argument("-ENDFdir","--ENDFdirectory",default="tungsten_test_ENDFB7.1",
help="Name of the data file for which the probability tables should be prepared.")
parser.add_argument("-T","--temperatures",default=293.15,type=float,nargs="+",
help="Temperature or temperatures for the processing of the neutron data.")
parser.add_argument("-p","--precision",default=6,type=int,choices=[1,2,3,4,5,6],
help="Precision (ipreci) for the production of probability tables in CALENDF.")
parser.add_argument("-pigzp","--pigzpath",default="0",
help="Absolute path to executable pigz.")
args=parser.parse_args()

calendfpath=args.calendfpath
if calendfpath=="0":
    print("The path to CALENDF was not set.")
    exit()
elif not os.path.exists(os.path.join(calendfpath,"xcalendf")):
    print('The path to CALENDF does not contain executable "xcalendf".')
    exit()
datafilename=args.ENDFdirectory
temperatures=args.temperatures
precision=args.precision

scriptpath=os.path.abspath(os.path.dirname(__file__))
datapath=os.path.join(scriptpath,datafilename)

if not os.path.exists(datapath):
    print('There is no datapath with the name "'+datafilename+'" in the directory with processing scripts.')
    exit()
elif not os.path.exists(datapath+"/CALENDF_run"):
    os.mkdir(datapath+"/CALENDF_run")

e_groups=np.genfromtxt(scriptpath+"/energy_groups_calendf.txt")

for fileiterator in os.scandir(datapath):
    if fileiterator.is_file():
        print("Processing "+fileiterator.name+".")
        e_limits=[-1,-1]
        #read Z and A from ENDF file
        with open(datapath+"/"+fileiterator.name,"r") as file:
            for line in file:
                if "1451" in line:
                    LRP=int(line[30:35].strip())
                    if LRP==0 or LRP==-1:
                        break
                    text=line[:12].strip()
                    if "+" in text:
                        isotopenumber=float(text.split("+")[0])*10**(int(text.split("+")[1]))
                    else:
                        isotopenumber=float(text)
                    Z=int(isotopenumber//1e3)
                    A=int(isotopenumber%1e3)
                    line=file.readline()
                    m=int(line[40:44].strip())
                    code=file.readline()[66:71].strip()
                    break
        # if the ENDF file does not have description of resonances -> continue
        if LRP==0 or LRP==-1:
            print("No description of resonances.")
            continue
        # read URR limits from the ENDF file
        with open(datapath+"/"+fileiterator.name, "r") as file:
            for line in file:
                if line[71]=="2":
                    while line[71]=="2":
                        splitted_line=line.split()
                        if (splitted_line[2]=="2" and not(int(splitted_line[4])>0 and int(splitted_line[4])%6==0)):
                            for s in range(2):
                                positive=splitted_line[s].split("+")
                                negative=splitted_line[s].split("-")
                                if len(positive)==2:
                                    e_limits[s]=(float(positive[0])*10**(int(positive[1])))
                                elif len(negative)==2:
                                    e_limits[s]=(float(negative[0])*10**(-1*int(negative[1])))
                            break
                        line=file.readline()
                    break
        # continue if there is no description of URR
        if e_limits[0]==-1:
            print("No description of URR.")
            continue
        # choose URR limits from the given energy structure
        for i in range(e_groups.size):
            if (e_limits[1] > e_groups[i+1] and e_limits[1]< e_groups[i]):
                e_max=e_groups[i]
            if e_limits[0] > e_groups[i]:
                e_min=e_groups[i]
                break
        # create content of new input file for each temperature
        for temp in temperatures:
            new_file_content=""
            calendf_name=str(Z)+"_"+str(A)+"_"+str(m)+"."+str(int(temp))+"."
            if os.path.exists(os.path.join(datapath,"CALENDF_run",calendf_name+"in")):
                if os.path.exists(os.path.join(datapath,"CALENDF_run",calendf_name+"tpc")):
                    continue
                else:
                    if os.path.exists(os.path.join(datapath,"CALENDF_run",calendf_name+"in")):
                        os.remove(os.path.join(datapath,"CALENDF_run",calendf_name+"in"))
                    if os.path.exists(os.path.join(datapath,"CALENDF_run",calendf_name+"sf")):
                        os.remove(os.path.join(datapath,"CALENDF_run",calendf_name+"sf"))
                    if os.path.exists(os.path.join(datapath,"CALENDF_run",calendf_name+"sft")):
                        os.remove(os.path.join(datapath,"CALENDF_run",calendf_name+"sft"))
                    if os.path.exists(os.path.join(datapath,"CALENDF_run",calendf_name+"tp")):
                        os.remove(os.path.join(datapath,"CALENDF_run",calendf_name+"tp"))
                    if os.path.exists(os.path.join(datapath,"CALENDF_run",calendf_name+"out")):
                        os.remove(os.path.join(datapath,"CALENDF_run",calendf_name+"out"))
            with open(scriptpath+"/calendf.in","r") as file:
                for line in file:
                    if ("ENERGIES" in line):
                        new_file_content+="ENERGIES "+"{:.3f}".format(e_min)+" "+"{:.3f}".format(e_max)+"\n"
                        continue
                    if ("TEFF" in line):
                        new_file_content+="TEFF "+str(temp)+"\n"
                        continue
                    if "NFEV" in line:
                        new_file_content+="NFEV 9 "+code+" '../"+fileiterator.name+"'"+"\n"
                        continue
                    if "sfr file" in line:
                        new_file_content+="NFSFRL  0 './"+calendf_name+"sfr'"+"\n"
                        continue
                    if "sf file" in line:
                        new_file_content+="NFSF   12 './"+calendf_name+"sf'"+"\n"
                        continue
                    if "sft file" in line:
                        new_file_content+="NFSFTP 11 './"+calendf_name+"sft'"+"\n"
                        continue
                    if "tp file" in line:
                        new_file_content+="NFTP   10 './"+calendf_name+"tp'"+"\n"
                        continue
                    if "tpc file" in line:
                        new_file_content+="NFTPR   17 './"+calendf_name+"tpc'"+"\n"
                        continue
                    if "IPRECI" in line:
                        new_file_content+="IPRECI "+str(precision)+"\n"
                        continue
                    else:
                        new_file_content+=line
            # create new input file
            new_file_name=os.path.join(datapath,"CALENDF_run",calendf_name+"in")
            with open(new_file_name,"w+") as new_file:
                new_file.write(new_file_content)
            # run CALENDF
            os.chdir(datapath+"/CALENDF_run")
            calendf=os.path.join(calendfpath,"xcalendf")
            print("Running CALENDF for "+calendf_name+"in...")
            os.system(calendf+" < "+calendf_name+"in > "+calendf_name+"out")

print("JOB DONE!")
